Ext.define('MyApp.view.main.Main', {
    extend: 'Ext.grid.Panel',

    controller: 'main',

    viewConfig: {
        markDirty: false
    },

    store: {
        type: 'mates',
        storeId: 'mates'
    },

    bbar: [
      {xtype: 'button', text: 'Create', handler: 'onCreate'}
    ],

    columns: {
        defaults: {
            flex: 1,
            align: 'center'
        },
        items: [
            { xtype: 'rownumberer', text: '#', flex: .15},
            { text: 'First Name', dataIndex: 'name.first'},
            { text: 'Last Name', dataIndex: 'name.last'},
            { text: 'Age', dataIndex: 'age'},
            { 
                text: 'Aggregator', 
                renderer: (value, metaData, record) => 
                    `${record.get('name.first')[0]}.${record.get('name.last')[0]}. - ${record.get('email')}`
            },
            { 
                text: 'Action', 
                xtype: 'widgetcolumn',
                widget: {
                    xtype: 'container',
                    bind: {
                        record: '{record}'
                    },
                    defaults: {
                        xtype: 'button',
                        flex: 1
                    },
                    height: 30,
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {text: 'Edit', handler: 'onEdit'},
                        {text: 'Delete', handler: 'onDelete'}
                    ],
                    setRecord: Ext.emptyFn
                },
                flex: .5
            }
        ]
    }
});