Ext.define('MyApp.view.main.MainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.main',

    require: [
    	'MyApp.view.matePanel.MatePanel'
    ],

    onCreate: () => {
        const win = new Ext.window.Window({
                title: 'Edit',
                items: {
                    xtype: 'matepanel'
                }
            });
        win.show();
    },

    onEdit: (el) => {
        const form = new MyApp.view.matePanel.MatePanel(),
            record = el.up().getViewModel().get('record');
        form.loadRecord(record);
        const win = new Ext.window.Window({
                title: 'Edit',
                items: form
            });
        win.show();
    },

    onDelete: function(el) {
    	Ext.Msg.confirm('Warning', 'Delete this record?', (answer) => {
    		if (answer === 'yes'){
    			const grid = this.getView(),
    				store = grid.getStore(),
                    record = el.up().getViewModel().get('record');
    			store.remove(record);
                grid.getView().refresh();
    		}
    	});
    }
});