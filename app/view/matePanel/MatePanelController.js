Ext.define('MyApp.view.matePanel.MateController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.matepanel',

    onSave: function() {
        const form = this.getView();
        if (form.getRecord()){
            form.updateRecord();
        } else {
            Ext.getStore('mates').add(form.getValues());
            form.up('window').close();
        }
    }
});