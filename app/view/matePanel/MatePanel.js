Ext.define('MyApp.view.matePanel.MatePanel', {
    extend: 'Ext.form.Panel',

    alias: 'widget.matepanel',

    controller: 'matepanel',

    padding: 10,

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    defaults: {
        xtype: 'textfield',
        labelAlign: 'top'
    },

    buttons: [{
        text: 'Save', handler: 'onSave', formBind: true
    }],

    items: [
        {
            fieldLabel: 'First name',
            name: 'name.first',
            validator: (val) => !Ext.isEmpty(val) ? true : 'First name must not be empty!'
        },
        {
            fieldLabel: 'Last name',
            name: 'name.last',
            validator: (val) => !Ext.isEmpty(val) ? true : 'Last name must not be empty!'
        },
        {
            xtype: 'numberfield',
            allowDecimals: false,
            fieldLabel: 'Age',
            name: 'age',
            validator: (val) => (!Ext.isEmpty(val) && val > 0) ? true:  'Age must not be empty and must be positive number!'
        }
    ]

});