Ext.define('MyApp.model.Mate', {
    extend: 'Ext.data.Model',

    idProperty: 'guid',

    fields: [
    	'age',
     	'email',
     	{
 		    name: 'name.first', 
			type: 'string',
			mapping: 'name.first'
     	},
     	{
     		name: 'name.last', 
			type: 'string',
			mapping: 'name.last'
     	}
    ],

    hasOne : {
        model: 'Name', 
        name: 'name', 
        associationKey: 'name'
    }
});