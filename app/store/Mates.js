Ext.define('MyApp.store.Mates', {
    extend: 'Ext.data.Store',
    alias: 'store.mates',
    autoLoad: true,

    model: 'MyApp.model.Mate',

    proxy: {
        type: 'ajax',
        url : '/mates.json',
        reader: {
        	type: 'json'
        }
    }
});